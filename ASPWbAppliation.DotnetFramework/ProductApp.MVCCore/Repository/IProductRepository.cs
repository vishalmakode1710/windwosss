﻿namespace ProductApp.MVCCore.Repository
{
    public class IProductRepository
    {
        List<Product> GetProducts();
        bool AddProduct(ProductApp product);
    }
}
